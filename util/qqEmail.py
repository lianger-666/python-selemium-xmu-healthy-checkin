# -*- coding = utf-8 -*-
# @Author : LXC
# @Time : 2022/1/18 14:49
# @software : PyCharm
# @FILE : qqEmail
# coding=utf-8
import logging
import smtplib
import config
from email.mime.text import MIMEText

logging.basicConfig(format="%(filename)s | %(lineno)s | %(asctime)s | %(levelname)s | %(message)s",
                    filename=config.file_name, filemode='a', level=logging.INFO, encoding='utf-8')

def email(msg_to,content):
    msg = MIMEText(content)
    msg['Subject'] = "健康打卡" # 主题
    msg['From'] = config.msg_from
    msg['To'] = msg_to
    s = smtplib.SMTP_SSL(config.smtp_server, config.port)  # 邮件服务器及端口号
    try:
        s.login(config.msg_from, config.passwd)
        s.sendmail(config.msg_from, msg_to, msg.as_string())
        logging.info(str(msg_to)+"发送成功")
    except Exception as e:
        logging.error(str(msg_to)+"发送失败 "+str(e))
    finally:
        s.quit()
