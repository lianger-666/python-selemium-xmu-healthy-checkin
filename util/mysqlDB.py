# -*- coding = utf-8 -*-
# @Author : LXC
# @Time : 2022/1/18 15:14
# @software : PyCharm
# @FILE : mysqlDB
import logging

import pymysql
import config
from po.user import User
from util import encode, qqEmail

logging.basicConfig(format="%(filename)s | %(lineno)s | %(asctime)s | %(levelname)s | %(message)s",
                    filename=config.file_name, filemode='a', level=logging.INFO, encoding='utf-8')


def get_users():
    try:
        db = pymysql.connect(host=config.db_home,
                             user=config.db_user,
                             password=config.db_pass,
                             database=config.db_name)
        cur = db.cursor()
        cur.execute(config.selectSql)
        results = cur.fetchall()
        cur.close()
        db.close()
        users = []
        for result in results:
            username = encode.des_decrypt(config.key, result[1])
            password = encode.des_decrypt(config.key, result[2])
            email = encode.des_decrypt(config.key, result[3])
            user = User(username,password,email)
            users.append(user)
        return users
    except Exception as e:
        qqEmail.email(config.msg_from, "连接数据库失败")
        logging.error("连接数据库失败 " + str(e))


def createatable():
    try:
        db = pymysql.connect(host=config.db_home,
                             user=config.db_user,
                             password=config.db_pass,
                             database=config.db_name)
        cur = db.cursor()
        cur.execute(config.createTable)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        logging.error("创建数据库失败 " + str(e))


def insert(username, password, email):
    try:
        db = pymysql.connect(host=config.db_home,
                             user=config.db_user,
                             password=config.db_pass,
                             database=config.db_name)
        cur = db.cursor()
        username = encode.des_encrypt(config.key, username).decode('utf-8')
        password = encode.des_encrypt(config.key, password).decode('utf-8')
        email = encode.des_encrypt(config.key, email).decode('utf-8')
        select_sql = '''
        SELECT * FROM `information` where username = '%s';
        ''' % (username)
        cur.execute(select_sql)
        results = cur.fetchall()
        if (len(results) != 0):
            update_sql = '''
            UPDATE `information` set password = '%s',email = '%s' where username = '%s';
            '''%(password,email,username)
            cur.execute(update_sql)
        else:
            insert_sql = '''
            INSERT INTO `information`(username, password,email ) VALUES ('%s', '%s', '%s');
             ''' % (username, password, email)
            print(insert_sql)
            cur.execute(insert_sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        logging.error("插入数据失败 " + str(e))