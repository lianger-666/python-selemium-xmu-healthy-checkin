# -*- coding = utf-8 -*-
# @Author : LXC
# @Time : 2022/1/18 11:03
# @software : PyCharm
# @FILE : config

# main.py配置
url = "https://xmuxg.xmu.edu.cn/xmu/login?app=214"
implicitly_wait_time = 3

# qqEmain.py配置
smtp_server = "smtp.qq.com"
port="465"
msg_from = '@qq.com'  # 发送方邮箱
passwd = ''  # 填入发送方邮箱的授权码
msg_to = '@qq.com'  # 收件人邮箱

# mysqlDB
db_home = 'localhost'
db_user = 'root'
db_pass = '123456'
db_name = 'health_check_in'
selectSql = '''
        SELECT * FROM information
             '''

createTable = '''
DROP TABLE IF EXISTS `information`;
CREATE TABLE `information`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;
'''

# logger
file_name = 'healthy-check-in.log'

key="xMu#H)s1"