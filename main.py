# -*- coding = utf-8 -*-
# @Author : LXC
# @Time : 2022/1/17 10:57
# @software : PyCharm
# @FILE : main.py
import logging
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import config
from util import qqEmail, mysqlDB

username = ""
password = ""
email = ""
try_open_time = 3
try_login_time = 3
try_click_to_checkin_time = 3
try_switch_handle_time = 3
try_click_my_form_time = 3

logging.basicConfig(format="%(filename)s | %(lineno)s | %(asctime)s | %(levelname)s | %(message)s",
                    filename=config.file_name, filemode='a', level=logging.INFO, encoding='utf-8')


def open(cnt):
    try:
        driver.get(config.url)
        driver.implicitly_wait(config.implicitly_wait_time)
        driver.maximize_window()
        driver.implicitly_wait(config.implicitly_wait_time)
        button_box = driver.find_element(by=By.CLASS_NAME, value="buttonBox")
        buttons = button_box.find_elements(by=By.TAG_NAME, value="button")
        for button in buttons:
            if button.text == "统一身份认证":
                button.click()
                driver.implicitly_wait(config.implicitly_wait_time)
                break
            else:
                continue
    except:
        # 重试
        if cnt < try_open_time:
            driver.refresh()
            time.sleep(config.implicitly_wait_time)
            cnt = cnt + 1
            logging.info("第" + str(cnt) + "次重试,open()")
            open(cnt)
        else:
            qqEmail.email(email, "打卡失败")
            logging.error(username + "打卡失败 open")
            sys.exit(0)


def login(username, password, cnt):
    try:
        driver.find_element(by=By.ID, value="username").send_keys(username)
        driver.find_element(by=By.ID, value="password").send_keys(password)
        driver.find_element(
            by=By.XPATH, value='.//button[@class="auth_login_btn primary full_width"]').click()
        driver.implicitly_wait(5)
    except:
        # 重试
        if cnt < try_login_time:
            driver.refresh()
            time.sleep(config.implicitly_wait_time)
            cnt = cnt + 1
            logging.info("第" + str(cnt) + "次重试,login()")
            login(username, password, cnt)
        else:
            qqEmail.email(email, "打卡失败")
            logging.error(username + "打卡失败 login")
            sys.exit(0)


def click_to_checkin(cnt):
    try:
        # 点击打卡
        driver.find_element(by=By.XPATH, value='.//div[@class="moreFont"]').click()
        time.sleep(config.implicitly_wait_time)
        buttons = driver.find_elements(
            by=By.XPATH, value='.//div[@class="app_child box_flex"]')
        for button in buttons:
            if str(button.text).__contains__("Daily Health Report 健康打卡"):
                driver.execute_script("arguments[0].click();", button)
                driver.implicitly_wait(5)
                break
            else:
                continue
    except:
        # 重试
        if cnt < try_click_to_checkin_time:
            driver.refresh()
            time.sleep(config.implicitly_wait_time)
            cnt = cnt + 1
            logging.info("第" + str(cnt) + "次重试,click_to_checkin()")
            click_to_checkin(cnt)
        else:
            qqEmail.email(email, "打卡失败")
            logging.error(username + "打卡失败 click_to_checkin")
            sys.exit(0)


def switch_handle(cnt):
    try:
        handles = driver.window_handles
        for handle in handles:
            driver.switch_to.window(handle)
            if driver.title == "Daily Health Report 健康打卡":
                break
            else:
                continue
    except:
        # 重试
        if cnt < try_switch_handle_time:
            driver.refresh()
            time.sleep(config.implicitly_wait_time)
            cnt = cnt + 1
            logging.info("第" + str(cnt) + "次重试,switch_handle()")
            switch_handle(cnt)
        else:
            qqEmail.email(email, "打卡失败")
            logging.error("没找到网页")
            sys.exit(0)

def click_my_form(cnt):
    try:
        # 点击我的表单
        my_frame = driver.find_element(
            by=By.XPATH, value='.//div[@class="gm-scroll-view"]')
        a = my_frame.find_element(by=By.XPATH, value='.//div[@class="tab"]')
        driver.implicitly_wait(config.implicitly_wait_time)
        a.click()
        driver.implicitly_wait(config.implicitly_wait_time)
        div = driver.find_element(by=By.XPATH, value='.//div[@data-name="select_1582538939790"]')
        span = div.find_element(by=By.XPATH, value='.//span').text
        if span != "是 Yes":
            i = div.find_element(by=By.XPATH, value='.//i')
            i.click()
            driver.find_element(by=By.CLASS_NAME, value="items-group").click()
        driver.find_element(
            by=By.XPATH, value='.//span[@class="form-save position-absolute"]').click()
        driver.switch_to.alert.accept()
    except:
        # 重试
        if cnt < try_click_my_form_time:
            driver.refresh()
            time.sleep(config.implicitly_wait_time)
            cnt += 1
            logging.info("第" + str(cnt) + "次重试,click_my_form()")
            click_my_form(cnt)
        else:
            qqEmail.email(email, "打卡失败")
            logging.error(username + "打卡失败 click_my_form")
            sys.exit(0)


def print_begin():
    logging.info(username + "开始打卡")


def print_success():
    logging.info(username + "打卡成功")


def main():
    print_begin()
    open(0)
    login(username, password, 0)
    click_to_checkin(0)
    time.sleep(config.implicitly_wait_time)
    switch_handle(0)
    click_my_form(0)
    print_success()


if __name__ == '__main__':
    results = mysqlDB.get_users()
    print(results)
    for result in results:
        try_open_time = 5
        try_login_time = 5
        try_click_to_checkin_time = 5
        try_switch_handle_time = 5
        try_click_my_form_time = 5
        username = result.username
        password = result.password
        email = result.email
        print(username,password,email)
        try:
            driver = webdriver.Chrome()
            main()
        except Exception as e:
            qqEmail.email(email, "打卡失败")
            logging.error(username + "打卡失败 webdriver.Chrome() " + str(e))
            sys.exit(0)
        driver.quit()