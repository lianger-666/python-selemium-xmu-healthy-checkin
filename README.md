# 项目介绍
本项目是厦门大学健康打卡项目，用来练手！！！

### 使用说明
1. 安装chrome浏览器
2. 安装chrome驱动
3. 新建数据库
4. 插入对应数据
5. 修改config.py
6. 运行main.py

### 技术架构
python + selenium + mysql + smtplib + logging

### 数据库表格式
- username <br> 打卡账户 
- password <br> 打卡密码
- eamil    <br> 失败通知邮箱